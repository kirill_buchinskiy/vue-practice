var app = new Vue({
    el: '#app',
    data: {
        candidates: [{
                name: "Ben",
                votes: 0
            },
            {
                name: "Din",
                votes: 0
            },
            {
                name: "Max",
                votes: 0
            },
            {
                name: "John",
                votes: 0
            },
            {
                name: "Kendrik",
                votes: 0
            },
            {
                name: "Eden",
                votes: 0
            },

        ]
    },
    methods: {
        resetVotes: function () {
            this.candidates.forEach(candidate => {
                candidate.votes = 0;
            });
        }
    },
    computed: {
        sortedCandidates: function () {
            var sortedCandidates = this.candidates.sort(function (a, b) {
                return b.votes - a.votes;
            });
            return sortedCandidates;
        }
    }
})